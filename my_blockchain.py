import datetime
import hashlib
import json
from flask import Flask, jsonify

# MARK: Create blockchain class

class Blockchain:

    # MARK: Constructor

    def __init__(self):
        self.chain = []
        self.createBlock(proof=1, previousHash = '0')

    # MARK: Create block

    def createBlock(self, proof, previousHash):
        block = {
            'index': len(self.chain) + 1,
            'timestamp': str(datetime.datetime.now()),
            'proof': proof,
            'previous_hash': previousHash
            }
        self.chain.append(block)
        return block

    # MARK: Print previous block

    def previousBlock(self):
        return self.chain[-1]

    # MARK: Proof Of Work

    def proofOfWork(self, previousProof):
        newProof = 1
        checkProof = False

        while checkProof is False:
            hashOperations = hashlib.sha256(str(newProof**2-previousProof**2).encode()).hexdigest()
            if hashOperations[:4] == '0000':
                checkProof = True
            else:
                newProof += 1

        return newProof

    # MARK: Hash

    def hash(self, block):
        encondedBlock = json.dumps(block, sort_keys=True).encode()
        return hashlib.sha256(encondedBlock).hexdigest()

    # MARK: Validate chain

    def validateChain(self, chain):
        previousBlock = chain[0]
        blockIndex = 1

        while blockIndex < len(chain):
            block = chain[blockIndex]
            if block['previous_hash'] != self.hash(previousBlock):
                return False

            previousBlock = previousBlock['proof']
            proof = block['proof']
            hashOperation = hashlib.sha256(str(proof**2 - previousBlock**2).encode()).hexdigest()

            if hashOperation[:4] != '0000':
                return False
            
            previousBlock = block
            blockIndex += 1
        
        return True


# MARK: Create Flask App

app = Flask(__name__)

# MARK: Create blockchain object

blockchain = Blockchain()


# MARK: Mine a new block

# MARK: Create route for mine block

@app.route('/mine/block', methods = ['GET'])

def mineBlock():
    previousBlock = blockchain.previousBlock()
    previousProof = previousBlock['proof']
    proof = blockchain.proofOfWork(previousProof)
    previousHash = blockchain.hash(previousBlock)
    block = blockchain.createBlock(proof, previousHash)
    
    response = {'message': 'New block mined!',
                'index': block['index'],
                'timestamp': block['timestamp'],
                'proof': block['proof'],
                'previous_hash': block['previous_hash']}
    
    return jsonify(response), 200

# MARK: Get chain route

@app.route('/chain', methods = ['GET'])

# MARK: Get chain

def displayChain():
    response = {'chain': blockchain.chain,
                'length': len(blockchain.chain)}
    
    return jsonify(response), 200

# MARK: Validate chain route

@app.route('/valid', methods = ['GET'])

def valid():
    valid = blockchain.validateChain(blockchain.chain)
    
    if valid:
        response = {'message': 'Blockchain is valid! it works'}
    else:
        response = {'message': 'Blockchain is not valid!!!! Taking care!'}
        
    return jsonify(response), 200

app.run(host = '0.0.0.0', port = 5000)